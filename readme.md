# Super2Do

## Description
Super2Do App adalah aplikasi untuk membuat daftar segala sesuatu yang harus kita lakukan, kita bisa memberikan tanda pada tugas yang sudah selesai dengan mudah.


## How to run:
```
  - clone the project
  - create .env file and fill DB_URL= with your mongo url
  - get all the package below
  - run: go run main.go
```

## Package
```
get all required package by run, go get
  -gin: github.com/gin-gonic/gin
  -mongoDB: go.mongodb.org/mongo-driver
  -env: github.com/joho/godotenv
```

## Postman Documentation Link
```
https://documenter.getpostman.com/view/11004312/UVR5poHA
```

## Collection runner result in collection_runner.json