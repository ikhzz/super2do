package services

import (
	"context"
	"fmt"
	"super2doapp/config"
	"super2doapp/models"

	"go.mongodb.org/mongo-driver/bson"
)

type DataServices struct {}

var dbConn = config.MongoDb()

func(d *DataServices) GetAll() ([]models.Note, error) {
	
	cursor, err := dbConn.Collection("all").Find(context.Background(), bson.D{})
	if err != nil {
		println(err)
	}
	
	var data []models.Note
	for cursor.Next(context.TODO()){
		var elem models.Note
		
		if err := cursor.Decode(&elem); err != nil {
			fmt.Println(err)
      return data, err
    }
		data = append(data, elem)
	}
	return data, nil
}

func (d *DataServices) GetFilter(f bool) ([]models.Note, error) {
	cursor, err := dbConn.Collection("all").Find(context.Background(), bson.D{})
	if err != nil {
		println(err)
	}
	
	var data []models.Note
	for cursor.Next(context.TODO()){
		var elem models.Note
		
		if err := cursor.Decode(&elem); err != nil {
			fmt.Println(err)
      return data, err
    }
		if f == elem.Status {
			data = append(data, elem)
		}
	}
	return data, nil
}

func (d *DataServices) Create(dt models.Note) bool {
	_, err := dbConn.Collection("all").InsertOne(context.Background(), dt)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func (d *DataServices) UpdateOne(dt models.Note) bool {
	_, err := dbConn.Collection("all").UpdateOne(context.Background(), bson.M{"_id": dt.Id}, bson.M{"$set": bson.M{"status":!dt.Status, "description":dt.Description}})
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func (d *DataServices) ActiveToComplete(b bool) bool {
	_, err := dbConn.Collection("all").UpdateMany(context.Background(), bson.M{"status": false}, bson.M{"$set": bson.M{"status": true}})
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func (d *DataServices) CompleteToDelete(b bool) bool {
	_, err := dbConn.Collection("all").DeleteMany(context.Background(), bson.M{"status":true},)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}