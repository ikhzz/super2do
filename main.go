package main

import (
	"super2doapp/controllers"

	"github.com/gin-gonic/gin"
)

var (controller controllers.DataController)



func main(){
	r := gin.Default()

	v1 := r.Group("v1") 
	{
		v1.GET("/", controller.GetAll)
		v1.POST("/", controller.Create)
		v1.PATCH("/", controller.UpdateOne)
		v1.PUT("/", controller.UpdateMany)
	}
	
	r.Run(":8000")
}