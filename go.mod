module super2doapp

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/joho/godotenv v1.4.0
	go.mongodb.org/mongo-driver v1.8.1
)
