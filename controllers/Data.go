package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"super2doapp/models"
	"super2doapp/services"

	"github.com/gin-gonic/gin"
)

type DataController struct {}

var dataService services.DataServices

func (d *DataController) GetAll(ctx *gin.Context){
	get := ctx.Query("status")
	var data []models.Note
	var err error

	if len(get) > 0 {
		queryBool, err := strconv.ParseBool(get)
		if err != nil {
			fmt.Println(err)
			ctx.JSON(http.StatusConflict, gin.H{
				"status": "failed to process request",
			})
		} else {
			data, err = dataService.GetFilter(queryBool)
			if err != nil {
				fmt.Println(err)
			}
		}
	} else {
		data, err = dataService.GetAll()
	}

	if err != nil {
		fmt.Println(err)
		ctx.JSON(http.StatusConflict, gin.H{
			"status": "failed to get the data",
		})
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": data,
		"number": len(data),
	})
}

func (d *DataController) Create(ctx *gin.Context) {
	var note models.Note
	err := ctx.ShouldBind(&note)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status": "failed to process request",
		})
		return
	}

	status := dataService.Create(note)
	if !status {
		ctx.JSON(http.StatusRequestTimeout, gin.H{
			"status": "failed to create note",
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status": "Note Created",
		"data": note.Description,
	})	
}

func (d *DataController) UpdateOne(ctx *gin.Context){
	var note models.Note
	err := ctx.ShouldBind(&note)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status": "failed to process request",
		})
		return
	}
	
	status := dataService.UpdateOne(note)
	if !status {
		ctx.JSON(http.StatusRequestTimeout, gin.H{
			"status": "failed to update",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": "Updated",
		"data": note,
	})
}

func (d *DataController) UpdateMany(ctx *gin.Context){
	var req struct {
		Status bool `json:"status" `
	}
	
	err := ctx.ShouldBind(&req)
	if err != nil {
		fmt.Println("err bind", err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status": "failed to process request",
		})
		return
	}

	if req.Status {	
		req.Status = dataService.CompleteToDelete(req.Status)
	} else {
		req.Status = dataService.ActiveToComplete(req.Status)
	}

	if !req.Status {
		ctx.JSON(http.StatusRequestTimeout, gin.H{
			"status": "Failed to update",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": "Update Success",
	})
}